import os
from glob import glob
import nibabel as nib
import torch
import numpy as np
import fnmatch
import random
from torchvision.transforms import Compose, Pad, CenterCrop, ToTensor, Resize, ToPILImage
from torchvision.transforms import functional as F
from torch.utils.data.dataset import Dataset

default_transform = Compose([ToPILImage(), Pad(60), CenterCrop((288,288))])
        
class PairedMRI(Dataset):
    """
    To train harmonization network
    """
    def __init__(self, dataset_dirs, data_names, orientations, mode='train'):
        self.mode = mode
        self.dataset_dirs = dataset_dirs
        self.data_names = data_names
        self.orientations = orientations
        self.imgs, self.dataset_ids = self._get_files()

    def _get_files(self):
        imgs = []
        for data_name in self.data_names:
            img_list = []
            dataset_ids = []
            for dataset_id, dataset_dir in enumerate(self.dataset_dirs):
                for orientation in self.orientations:
                    full_path = os.path.join(dataset_dir, self.mode, f'*{data_name}*{orientation.upper()}*.nii.gz')
                    for img_path in sorted(glob(full_path)):
                        img_list.append(img_path)
                        dataset_ids.append(dataset_id)
            imgs.append(img_list)
        return tuple(imgs), dataset_ids
        
    def __len__(self):
        return len(self.imgs[0])

    def __getitem__(self, idx:int):
        imgs = []
        other_imgs = []
        for modality_id in range(len(self.imgs)):
            img_path = self.imgs[modality_id][idx]
            img = nib.load(img_path).get_fdata().astype(np.float32).transpose([1,0])
            msk = img == 0
            img = default_transform(img)
            img = ToTensor()(np.array(img))
            dataset_id = self.dataset_ids[idx]

            if 'AXIAL' in img_path:
                str_id = img_path.find('_AXIAL')
            elif 'CORONAL' in img_path:
                str_id = img_path.find('_CORONAL')
            else:
                str_id = img_path.find('_SAGITTAL')
            pattern = img_path[:str_id]+'*AXIAL_SLICE11*.nii.gz'    # same subject but different slice or orientation
            pattern2 = img_path[:str_id]+'*AXIAL_SLICE12*.nii.gz'    # same subject but different slice or orientation
            #print(pattern)
            other_img_path = random.choice(fnmatch.filter(self.imgs[modality_id], pattern)+fnmatch.filter(self.imgs[modality_id], pattern2))
            other_img = nib.load(other_img_path).get_fdata().astype(np.float32).transpose([1,0])
            other_img = default_transform(other_img)
            other_img = ToTensor()(np.array(other_img))
            imgs.append(img)
            other_imgs.append(other_img)

        # make sure both T1 and T2 have the same FOV
        img0 = imgs[0]
        img1 = imgs[1]
        msk0 = img0.ge(1e-3)
        msk1 = img1.ge(1e-3)
        msk = msk0 & msk1
        imgs[0][~msk] = 0
        imgs[1][~msk] = 0
        return tuple(imgs), dataset_id, tuple(other_imgs)


