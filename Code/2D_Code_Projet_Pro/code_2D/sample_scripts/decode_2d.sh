#!/bin/bash

python ../decode_2d.py \
    --in-beta-dir directory_that_stores_beta_images \
    --in-theta directory_that_stores_theta_file \
    --data-name T1 \
    --out-dir output_dir \
    --pretrained-model directory_of_pretrained_model \
    --gpu 1 \
    --beta-dim 4 \
    --theta-dim 2 
