#!/bin/bash

python ../train_harmonization.py \
    --dataset-dirs data/siteA data/siteB
    --data-names T1 T2 \
    --orientation AXIAL \
    --epochs 5 \
    --gpu 0 \
    --batch-size 6 \
    --out-dir output_dir
