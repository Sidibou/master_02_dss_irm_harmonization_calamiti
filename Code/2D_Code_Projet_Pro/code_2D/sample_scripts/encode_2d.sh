#!/bin/bash

python ../encode_2d.py \
    --in-dir data/siteA/test
    --data-name T1 \
    --orientation AXIAL \
    --out-dir output_dir \
    --pretrained-model dir_to_pretrained_model \
    --gpu 1 
