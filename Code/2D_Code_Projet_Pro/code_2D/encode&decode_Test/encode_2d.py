#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
from glob import glob
import argparse
import numpy as np
from modules.model import CALAMITI

def obtain_single_img(img_dir):
    import torch
    import nibabel as nib
    from PIL import Image
    from torchvision.transforms import Compose, Pad, CenterCrop, ToTensor, Resize
    
    img = nib.load(img_dir).get_fdata().squeeze().transpose([1,0])
    img = Image.fromarray(img)
    transform = Compose([Pad(20), CenterCrop((288,288))])
    img = transform(img)
    return ToTensor()(np.array(img))

def main(args=None):
    args = sys.argv[1:] if args is None else args
    parser = argparse.ArgumentParser(description='DisentangledVAE')
    parser.add_argument('--in-dir', type=str, required=True)
    parser.add_argument('--out-dir', type=str, required=True)
    parser.add_argument('--data-name', type=str, required=True)
    parser.add_argument('--orientation', type=str, required=True)
    parser.add_argument('--pretrained-model', type=str, required=True)
    parser.add_argument('--beta-dim', type=int, default=4)
    parser.add_argument('--theta-dim', type=int, default=2)
    parser.add_argument('--gpu', type=int, default=0)
    parser.add_argument('--fine-tune',  default=False, action='store_true')
    args = parser.parse_args(args)

    # initialize model
    encoder = CALAMITI(beta_dim = args.beta_dim,
                       theta_dim = args.theta_dim,
                       train_sample = 'st_gumbel_softmax',
                       valid_sample = 'argmax',
                       pretrained_model = args.pretrained_model,
                       gpu = args.gpu,
                       fine_tune = args.fine_tune)

    # obtain images
    imgs = []
    full_path = os.path.join(args.in_dir, f'*{args.data_name}*{args.orientation}*.nii.gz')
    for img_path in sorted(glob(full_path)):
        imgs.append(img_path)

    # encode images
    for img_id, img_path in enumerate(imgs):
        print('Processing:', str(img_id)+'/'+str(len(imgs)))
        img = obtain_single_img(img_path)
        str_id = img_path.find('.nii.gz')
        prefix = f'{img_path[len(args.in_dir) : str_id]}'
        encoder.encode_single_img(img, img, args.out_dir, prefix)
        
        
if __name__ == '__main__':
    main()
