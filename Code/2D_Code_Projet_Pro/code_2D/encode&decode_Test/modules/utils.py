import torch
from torch.nn import functional as F
from torch import nn
import numpy as np

def softmax(logits, temperature=1.0, dim=1):
    exps = torch.exp(logits/temperature)
    return exps/torch.sum(exps, dim=dim)

def create_one_hot(soft_prob, dim):
    indices = torch.argmax(soft_prob, dim=dim)
    hard = F.one_hot(indices, soft_prob.size()[dim])
    new_axes = tuple(range(dim)) + (len(hard.shape)-1,) + tuple(range(dim, len(hard.shape)-1))
    return hard.permute(new_axes).float()

class KLDivergenceLoss(nn.Module):
    def __init__(self, reduction='none'):
        super(KLDivergenceLoss, self).__init__()
        self.reduction = reduction
    def forward(self, mu, logvar):
        kld = -0.5 * logvar + 0.5 * (torch.exp(logvar) + torch.pow(mu,2)) - 0.5
        if self.reduction == 'mean':
            kld = kld.mean()
        return kld

# http://stackoverflow.com/a/22718321
def mkdir_p(path):
    import os
    import errno
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

class TemperatureAnneal:
    def __init__(self, initial_temp=1.0, anneal_rate=0.0, min_temp=0.5, device=torch.device('cuda')):
        self.initial_temp = initial_temp
        self.anneal_rate = anneal_rate
        self.min_temp = min_temp
        self.device = device

        self._temperature = self.initial_temp
        self.last_epoch = 0

    def get_temp(self):
        return self._temperature

    def step(self, epoch=None):
        if epoch is None:
            epoch = self.last_epoch + 1
        self.last_epoch = epoch
        current_temp = self.initial_temp * np.exp(-self.anneal_rate * self.last_epoch)
        # noinspection PyArgumentList
        self._temperature = torch.max(torch.FloatTensor([current_temp, self.min_temp]).to(self.device))

    def reset(self):
        self._temperature = self.initial_temp
        self.last_epoch = 0

    def state_dict(self):
        return {key: value for key, value in self.__dict__.items()}

    def load_state_dict(self, state_dict):
        self.__dict__.update(state_dict)
