#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import argparse
import torch
from glob import glob
import numpy as np
from PIL import Image, ImageOps
import nibabel as nib
from torchvision.transforms import ToTensor
   
from modules.model import CALAMITI


def load_beta(beta_dir):
    beta = []
    for img_dir in beta_dir:
        nii_file = nib.load(img_dir)
        beta_temp = nii_file.get_fdata().transpose([1,0])
        beta_temp = Image.fromarray(beta_temp)
        beta.append(ToTensor()(np.array(beta_temp)).unsqueeze(0))
    return torch.cat(beta, dim=1), nii_file.header, nii_file.affine

def load_theta(theta_dir):
    try:
        theta = torch.FloatTensor([float(value) for value in theta_dir])
    except ValueError:
        theta = torch.FloatTensor(np.loadtxt(theta_dir, delimiter=',')).unsqueeze(0).unsqueeze(2).unsqueeze(3)
        #print(theta.shape)
    #theta = torch.from_numpy(np.loadtxt(theta_dir)).unsqueeze(0).unsqueeze(0).unsqueeze(2).unsqueeze(3)
    #theta = theta.float()
    return theta

def main(args=None):
    args = sys.argv[1:] if args is None else args
    parser = argparse.ArgumentParser(description='DisentangledVAE')
    parser.add_argument('--in-beta-dir', type=str, required=True)
    parser.add_argument('--in-theta', type=str, required=True)
    parser.add_argument('--out-dir', type=str, required=True)
    parser.add_argument('--pretrained-model', type=str, default=None)
    parser.add_argument('--data-name', type=str, required=True)
    parser.add_argument('--beta-dim', type=int, default=4)
    parser.add_argument('--theta-dim', type=int, default=2)
    parser.add_argument('--gpu', type=int, default=0)
    args = parser.parse_args(args)

    # initialize model
    decoder = CALAMITI(beta_dim = args.beta_dim,
                              theta_dim = args.theta_dim,
                              train_sample = 'st_gumbel_softmax',
                              valid_sample = 'argmax',
                              pretrained_model = args.pretrained_model,
                              gpu = args.gpu)

    # obtain a list of beta
    beta_paths = []
    beta_full_path = os.path.join(args.in_beta_dir, f'*{args.data_name}*beta_channel0.nii.gz')
    for beta_path in sorted(glob(beta_full_path)):
        beta_paths.append(beta_path)
    num_subs = len(beta_paths)
    
    for sub_id in range(num_subs):
        print('Processing:', str(sub_id+1)+'/'+str(num_subs))
        beta_path = beta_paths[sub_id]
        str_id = beta_path.find('_beta_channel0.nii.gz')
        prefix = beta_path[len(args.in_beta_dir)+1:str_id]

        betas = []
        for ch in range(args.beta_dim):
            beta = os.path.join(args.in_beta_dir, f'{prefix}_beta_channel{str(ch)}.nii.gz')
            betas.append(beta)
        beta, hdr, affine = load_beta(betas)
        theta = load_theta(args.in_theta)
        #print(beta.shape, theta.shape)
        #print(prefix)
        decoder.decode_single_img(beta, theta, args.out_dir, prefix, img_hdr=hdr, img_affine=affine)

if __name__ == '__main__':
    main()
