#!/usr/bin/env python
# coding: utf-8

# In[2]:


import os
import numpy as np
import nibabel as nib




# In[6]:


def _3D_to_2D(input_dir,output_dir) :
    
    
    # Boucle à travers chaque fichier IRM en 3D dans le dossier d'entrée
    for file_name in os.listdir(input_dir):
        if file_name.endswith(".nii.gz"):
            # Charger le fichier IRM en 3D
            img = nib.load(os.path.join(input_dir, file_name))
            data = img.get_fdata()

            # Générer les images axiales
            for i in range(data.shape[2]):
                axial_slice = data[:, :, i]
                axial_img = nib.Nifti1Image(axial_slice, img.affine)
                nib.save(axial_img, os.path.join(output_dir, f"{file_name.split('.')[0]}_AXIAL_slice-{i}.nii.gz"))

            # Générer les images sagittales
            for i in range(data.shape[0]):
                sagittal_slice = data[i, :, :]
                sagittal_img = nib.Nifti1Image(sagittal_slice, img.affine)
                nib.save(sagittal_img, os.path.join(output_dir, f"{file_name.split('.')[0]}_SAGITTAL_slice-{i}.nii.gz"))

            # Générer les images coronales
            for i in range(data.shape[1]):
                coronal_slice = data[:, i, :]
                coronal_img = nib.Nifti1Image(coronal_slice, img.affine)
                nib.save(coronal_img, os.path.join(output_dir, f"{file_name.split('.')[0]}_CORONAL_slice-{i}.nii.gz"))


# In[7]:


# Définir le chemin du dossier contenant les fichiers IRM en 3D
input_dir = "./WhiteStripe_t1"

# Définir le chemin du dossier où les images seront enregistrées
output_dir = "./Data/Site_A"


# In[ ]:


_3D_to_2D(input_dir,output_dir)


# In[ ]:




