import pandas as pd
import os
from os import listdir
from os.path import isfile, join
from subprocess import run

def preprocessing(path_irm, 
                  N4BIASfield_out, 
                  t2_to_t1_out,
                  mat_t2,template, 
                  flirt_t1_out,
                  mat_t1, 
                  flirt_t2_out,
                  new_mat_t2):

    # Appliquer la correction N4 sur toutes les images T1 et T2 dans path_irm
    i=1
    for filename in os.listdir(path_irm):
        input_file = os.path.join(path_irm, filename)
        output_file = os.path.join(N4BIASfield_out, filename)
        cmd="N4BiasFieldCorrection -i "+input_file+" -o "+output_file
        run(cmd.split(" "))
        print("N4BiasFieldCorrection : ",i)
        i+=1
        
    # Transformer les images T2 en images T1, si nécessaire
    i=1
    for filename in os.listdir(path_irm):   

        if "t2" in filename:
            t1_irm = os.path.join(N4BIASfield_out,filename.replace("t2", "t1"))
            t2_irm = os.path.join(N4BIASfield_out, filename)
            MAT_T2 = os.path.join(mat_t2, filename.replace(".nii.gz",".mat"))
            new_irm_t2 = os.path.join(t2_to_t1_out, filename)
                                  
            cmd="flirt -in "+ t2_irm +" -ref "+ t1_irm + " -omat " + MAT_T2 + " -interp trilinear -dof 6 -out "+new_irm_t2
            run(cmd.split(" "))
            print("T2_to_T1 : ",i)
            i+=1

    # Appliquer le flirt sur toutes les images T1 et T2 dans path_irm
    i=1
    for filename in os.listdir(path_irm):  

        if "t2" in filename:

            t2_irm = os.path.join(t2_to_t1_out, filename)
            new_MAT_T2 = os.path.join(new_mat_t2, filename.replace(".nii.gz",".mat"))
            flirt_irm_t2 = os.path.join(flirt_t2_out, filename)
            MAT_T1 = os.path.join(mat_t1, filename.replace("_t2.nii.gz","_t1.mat"))
            cmd="flirt -applyxfm -in "+ t2_irm +" -ref "+ template + " -init " + MAT_T1 + " -interp trilinear -dof 6 -out "+ flirt_irm_t2
            run(cmd.split(" "))
            print("Flirt T2 : ",i)
            i+=1
        else :    
            t1_irm = os.path.join(N4BIASfield_out, filename)
            MAT_T1 = os.path.join(mat_t1, filename.replace(".nii.gz",".mat"))
            flirt_irm_t1 = os.path.join(flirt_t1_out, filename)
            cmd="flirt -in "+ t1_irm +" -ref "+ template + " -omat " + MAT_T1 + " -interp trilinear -dof 6 -out "+ flirt_irm_t1
            run(cmd.split(" "))
            print("Flirt T1 : ",i)
            i+=1
            

def  flirt_mask(mask_path,template_path,mat_path,dest_path) :
    # flirt -in <mask_path> -ref <template_path> -init <mat_path> -applyxfm -interp nearestneighbour -out <dest_path>
    i=1
    for filename in os.listdir(mask_path):
        input_file = os.path.join(mask_path, filename)
        output_file = os.path.join(dest_path, filename)
        MAT_T1 = os.path.join(mat_t1, filename.replace(".nii.gz","_t1.mat"))
        cmd="flirt -applyxfm -in "+ input_file +" -ref "+ template + " -init " + MAT_T1 + " -interp trilinear -dof 6 -out "+ output_file

        run(cmd.split(" "))
        #print(">$ :",cmd)
        print("Flirt Mask : ",i)
        i+=1
        

def WhiteStripe(IRM,mask_path,WhiteStripe_out):
    i=1
    for filename in os.listdir(IRM):  
        
        if "t2" in filename:
            
            t2_irm = os.path.join(IRM, filename)
            mask = os.path.join(mask_path, filename.replace("_t2.nii.gz",".nii.gz"))
            WhiteStripe = os.path.join(WhiteStripe_out, filename)
            cmd ="ws-normalize "+t2_irm+" -m "+mask+" -o "+WhiteStripe+" -v"            
            run(cmd.split(" "))
            #print(">$ :",cmd)
            print("WhiteStripe T2 : ",i)
            i+=1
        else :    
            t1_irm = os.path.join(IRM, filename)
            mask = os.path.join(mask_path, filename.replace("_t1.nii.gz",".nii.gz"))
            WhiteStripe = os.path.join(WhiteStripe_out, filename)
            cmd ="ws-normalize "+t1_irm+" -m "+mask+" -o "+WhiteStripe+" -v"            
            run(cmd.split(" "))
            #print(">$ :",cmd)
            print("WhiteStripe T1: ",i)
            i+=1