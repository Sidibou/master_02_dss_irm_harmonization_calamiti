from preprocessing import *


# Correction d'inhomogénéités :
# commande : N4BiasFieldCorrection -i <mri_path> -o <dest_path>
# Registration espace MNI :

path_irm = "./data"                  
N4BIASfield_out = "./N4B"
t2_to_t1_out="./T2_to_T1"
mat_t2 = "./mat_t2"
template = "mni_icbm152_t1_tal_nlin_sym_09a.nii" 
flirt_t1_out ="./flirt_t1"
mat_t1 = "./mat_t1"
flirt_t2_out = "./flirt_t2"
new_mat_t2 = "./new_mat_t2"

preprocessing(path_irm, N4BIASfield_out, t2_to_t1_out, mat_t2, template, flirt_t1_out, mat_t1, flirt_t2_out, new_mat_t2)


# Mask to espace MNI :

mask_path = "./mask"
template = "mni_icbm152_t1_tal_nlin_sym_09a.nii"  
mat_path = "./mat_mask"
dest_path='./flirt_mask'


flirt_mask(mask_path,template,mat_path,dest_path)


# Normalisation White-stripe

WhiteStripe_t1_out="./WhiteStripe_t1"
WhiteStripe_t2_out="./WhiteStripe_t2"
flirt_t1_out ="./flirt_t1"
flirt_t2_out = "./flirt_t2"


WhiteStripe(flirt_t1_out,dest_path,WhiteStripe_t1_out)
WhiteStripe(flirt_t2_out,dest_path,WhiteStripe_t2_out)