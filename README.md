# Dépôt Git - Travaux réalisés

## Plan :

1. Pre-Processing :

   - 1.1 - **data** : contient les données brutes (IRM avant les 3 étapes de prétraitement)
   - 1.2 - **preprocessing.py** : le script qui permet de réaliser les 3 étapes et de générer les résultats de chaque étape dans le dossier correspondant (nom du dossier)
   - 1.3 - **preprocessing_Exec.py** : un script facile à utiliser, il suffit de saisir les chemins des dossiers et le template + masks
   - 1.4 - **Show_Slices.ipynb** : permet de visualiser les résultats de prétraitement
   - 1.5 - **mni_icbm152_t1_tal_nlin_sym_09a.nii** : le template utilisé pour la deuxième étape (Registration to MNI space)

   Note : le script **preprocessing_Exec.py** nous permet de modifier et choisir le template sans changer le code.

2. **3D_Transformation_Exec.py** : script Python permettant de générer 280 images 2D (80 axiales, 100 coronales et 100 sagittales) à partir d'une IRM 3D

   Pour éviter que la taille de ce dépôt ne soit trop volumineuse (80 * 280 = 22 400), nous avons laissé seulement 4 IRM 3D par site. Cependant, pour lancer l'apprentissage, il faut relancer ce script. Les IRM 2D résultantes doivent être enregistrées dans le dossier "2D_Code_Projet_Pro\Cleaned_Data" divisé par site (A et B), type (2D et 3D) et utilité (données d'entraînement et données de test).

3. **2D_Code_Projet_Pro** : Contient le code modifié et corrigé qui permet de :

   - réaliser l'apprentissage (**Training.ipynb**) : les modèles se trouvent dans le dossier "2D_Code_Projet_Pro\code_2D\models"
   - effectuer l'encodage et le décodage (**2D_Code_Projet_Pro\code_2D\encode&decode_Test**)

4. **3D_Code_Projet_Pro** :

   - Modèle 3D entraîné (**3D_Code_Projet_Pro\models**) : pour utiliser ce modèle, il faut utiliser les données fournies sur le site CALAMITI comme template dans la deuxième étape.
   - Scripts **Encode_data** et **Decode_data** : permettent de tester le modèle
   - **test_decode** : les images générées après harmonisation
   - **Show_Slices** : visualisation des IRM avant et après harmonisation

## Référence :

  - Lianrui Zuo, Blake E. Dewey, Yihao Liu, Yufan He, Scott D. Newsome, Ellen M. Mowry, Susan M. Resnick, Jerry L. Prince, Aaron Carass,
Unsupervised MR harmonization by learning disentangled representations using information bottleneck theory,
NeuroImage, Volume 243, 2021, 118569, ISSN 1053-8119, [https://doi.org/10.1016/j.neuroimage.2021.118569](https://doi.org/10.1016/j.neuroimage.2021.118569).

Site web : [https://iacl.ece.jhu.edu/index.php?title=CALAMITI](https://iacl.ece.jhu.edu/index.php?title=CALAMITI)

  - IXI Brain Development Dataset. https://brain-development.org/ixi-dataset/  Accessed: 2019-12-10
